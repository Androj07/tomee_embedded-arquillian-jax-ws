package com.junior.beans;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;

@Stateless
@Default
public class ChampionSSB implements Motivated {

	@Override
	public String whoAmI() {
		return "I am the champion";
	}

}
