package com.junior.smartphones.dao;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.junior.smartphones.data.Smartphone;

@Singleton
public class SmartphonesDao {

	@PersistenceContext
	private EntityManager em;

	public List<Smartphone> findAll() {
		TypedQuery<Smartphone> findAllSmartphones = em.createNamedQuery(Smartphone.FIND_ALL, Smartphone.class);
		return findAllSmartphones.getResultList();
	}

}
