package com.junior.smartphones.ws;

import java.util.List;

import javax.jws.WebService;

import com.junior.smartphones.data.Smartphone;

@WebService(targetNamespace = "http://com.junior/wsdl")
public interface PhonesWs {
	List<Smartphone> getSmartphones();
}
