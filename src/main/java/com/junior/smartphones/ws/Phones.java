package com.junior.smartphones.ws;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import com.junior.smartphones.dao.SmartphonesDao;
import com.junior.smartphones.data.Smartphone;

@Stateless
@WebService(portName = "PhonesPort", serviceName = "PhonesService", targetNamespace = "http://com.junior/wsdl", endpointInterface = "com.junior.smartphones.ws.PhonesWs")
public class Phones implements PhonesWs {

	@Inject
	private SmartphonesDao smartphonesDao;

	@Override
	public List<Smartphone> getSmartphones() {
		return smartphonesDao.findAll();
	}

}
