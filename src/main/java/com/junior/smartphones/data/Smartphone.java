package com.junior.smartphones.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "SMARTPHONES_TABLE")
@NamedQueries({ @NamedQuery(name = Smartphone.FIND_ALL, query = "Select s From Smartphone s") })
public class Smartphone {

	public final static String FIND_ALL = "smartphone.findAll";

	@Id
	private Long id;
	private String model;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Smartphone [id=" + id + ", model=" + model + "]";
	}

}
