package com.junior.beans.alternatives;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import com.junior.beans.Motivated;

@Alternative
@Stateless
public class AlternativeSSB implements Motivated {

	@Override
	public String whoAmI() {
		return "I am an Alternative";
	}

}
