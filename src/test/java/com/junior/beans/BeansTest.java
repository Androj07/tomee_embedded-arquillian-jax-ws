package com.junior.beans;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.junior.beans.alternatives.AlternativeSSB;

@RunWith(Arquillian.class)
public class BeansTest {

	@Inject
	Motivated cut;

	@Deployment(name = "withAlternative")
	public static Archive<?> deployWithAlternative() {
		WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "alternatived.war")
				.addClasses(Motivated.class, ChampionSSB.class, AlternativeSSB.class)
				.addAsWebInfResource("beans-with-alternative.xml", "beans.xml");
		System.out.println(webArchive.toString(true));
		return webArchive;
	}

	@Test
	@OperateOnDeployment("withAlternative")
	public void shouldReturnTextFromAlternative() {
		assertThat(cut.whoAmI(), equalTo("I am an Alternative"));
	}

}
