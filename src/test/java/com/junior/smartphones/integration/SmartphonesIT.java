package com.junior.smartphones.integration;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.junior.smartphones.dao.SmartphonesDao;
import com.junior.smartphones.data.Smartphone;
import com.junior.smartphones.ws.PhonesWs;

@RunWith(Arquillian.class)
public class SmartphonesIT {

	@Inject
	private SmartphonesDao cut;

	@Deployment
	public static Archive<?> deployment() {
		WebArchive war = ShrinkWrap.createFromZipFile(WebArchive.class, new File("target/smartphones.war"));

		war.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		war.addAsWebInfResource("test-persistence.xml", "persistence.xml");
		// war.addAsWebInfResource("web.xml");
		// war.addAsWebInfResource("sun-jaxws.xml");
		war.delete("META-INF/maven/");
		System.out.println(war.toString(true));
		return war;
	}

	private void runSqlScript(String scriptName) {
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			Connection mConnection = DriverManager.getConnection("jdbc:hsqldb:mem:arquillian?user=sa&password=");
			ScriptRunner runner = new ScriptRunner(mConnection);
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(scriptName).getFile());
			runner.runScript(new BufferedReader(new FileReader(file)));
		} catch (ClassNotFoundException e) {
			System.err.println("Unable to get mysql driver: " + e);
		} catch (SQLException e) {
			System.err.println("Unable to connect to server: " + e);
		} catch (FileNotFoundException e) {
			System.err.println("Script not found: " + e);
		}
	}

	@ArquillianResource
	URL url;

	@Before
	public void setUp() {
		this.runSqlScript("init.sql");
	}

	@After
	public void cleanUp() {
		this.runSqlScript("drop.sql");
	}

	@Test
	public void shouldReturn3Smartphones() {
		// given

		// when
		List<Smartphone> all = cut.findAll();
		// then
		assertThat(all, notNullValue());
		assertThat(all.size(), equalTo(3));
	}

	@Test
	public void shouldReturnSmartphonesFromWS() throws MalformedURLException {
		// given
		Service phonesService = Service.create(new URL(url.toString() + "webservices/Phones?wsdl"),
				new QName("http://com.junior/wsdl", "PhonesService"));
		PhonesWs phonesWs = phonesService.getPort(PhonesWs.class);
		// when
		List<Smartphone> all = phonesWs.getSmartphones();
		// then
		assertThat(all, notNullValue());
		assertThat(all.size(), equalTo(3));
	}
}
